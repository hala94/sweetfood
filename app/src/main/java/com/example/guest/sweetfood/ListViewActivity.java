package com.example.guest.sweetfood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity {

    ArrayList<FoodRecipe> dataModel;
    CustomAdapter listAdapter;
    ListView listView;

    private ArrayList<FoodRecipe> configureDataModel() {
        ArrayList<FoodRecipe> dataSource = new ArrayList<FoodRecipe>();
        int[]images={R.drawable.pumpkinpie,R.drawable.avocadomousse,R.drawable.crumbleslice,
                R.drawable.coconutpie,R.drawable.carrotcupcakes,R.drawable.oatmealcookies,R.drawable.parfaitsblue,
                R.drawable.vanilla};
        String[]names={"Vegan pumpkin pie","Avocado Mousse Tart","Blackberry crumble slice",
                "Coconut pie","Carrot cupcakes","Oatmeal cookies","Bluebery parfaits","Vanilla cheesecake"};

        for (int i=0; i<names.length; i++) {
            FoodRecipe foodRecipeObject = new FoodRecipe(names[i],images[i]);
            dataSource.add(foodRecipeObject);
        }

        return dataSource;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        this.dataModel = configureDataModel();
        this.listAdapter = new CustomAdapter();


        listView=findViewById(R.id.recipe_list);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView customAdapter, View view, int position, long id) {
                FoodRecipe foodRecipe = FoodRecipe.class.cast(listAdapter.getItem(position));
                Intent intent = new Intent(ListViewActivity.this, RecipeDetailActivity.class);
                intent.putExtra("FoodRecipe", foodRecipe);
                startActivityForResult(intent,0);

//                if(position==0)
//                {
//                    Intent intentt=new Intent(view.getContext(),PumpkinClass.class);
//                    startActivityForResult(intentt,0);
//                }
//
//
//                if(position==1)
//                {
//                    Intent intentt=new Intent(view.getContext(),AvocadoClass.class);
//                    startActivityForResult(intentt,1);
//                }
//
//
//                if(position==2)
//                {
//                    Intent intentt=new Intent(view.getContext(),CrumbleClass.class);
//                    startActivityForResult(intentt,2);
//                }
//
//                if(position==3)
//                {
//                    Intent intentt=new Intent(view.getContext(),CoconutClass.class);
//                    startActivityForResult(intentt,3);
//                }
//
//                if(position==4)
//                {
//                    Intent intentt=new Intent(view.getContext(),CarrotClass.class);
//                    startActivityForResult(intentt,4);
//                }
//
//                if(position==5)
//                {
//                    Intent intentt=new Intent(view.getContext(),OatmealClass.class);
//                    startActivityForResult(intentt,5);
//                }
//                if(position==6)
//                {
//                    Intent intentt=new Intent(view.getContext(),ParfaitsClass.class);
//                    startActivityForResult(intentt,6);
//                }
//
//                if(position==7)
//                {
//                    Intent intentt=new Intent(view.getContext(),VanillaClass.class);
//                    startActivityForResult(intentt,7);
//                }


            }
        });


    }


    class CustomAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return dataModel.size();
        }

        @Override
        public Object getItem(int position) {
            return dataModel.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position,View convertView,ViewGroup parent) {
            FoodRecipe foodRecipe = FoodRecipe.class.cast(listAdapter.getItem(position));
            View foodRecipeView=getLayoutInflater().inflate(R.layout.customlayout,null);
            ImageView imageView= foodRecipeView.findViewById(R.id.imageView);
            TextView textView= foodRecipeView.findViewById(R.id.textView);
            imageView.setImageResource(foodRecipe.imageId);
            textView.setText(foodRecipe.recipeName);
            return foodRecipeView;
        }
    }




    }

